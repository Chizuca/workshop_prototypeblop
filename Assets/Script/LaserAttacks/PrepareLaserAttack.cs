﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PrepareLaserAttack : LaserAttacks
{
    public LaserAttacks nextAttack;
    public LaserAttacks laserMovement;

    public PrepareLaserAttack(BossBehaviour bossBehaviour, GameObject laser, float laserSize, LaserAttacks nextAttack) : base(bossBehaviour, laser, nextAttack.timeTaken, laserSize)
    {
        this.nextAttack = nextAttack;

        laserMovement = nextAttack.ShallowCopy();
        laserMovement.laser = this.laser;

        laserMovement.laserSize /= 2;
        laserMovement.triggerOnRemove += Remove;
    }

    public override void OnAdded()
    {
       
    }

    public override void Tick()
    {
        base.Tick();

        laserMovement?.Tick();
    }

    public override void Remove()
    {
        bossBehaviour.AddLaserAttack(nextAttack);
        nextAttack = null;
        base.Remove();
    }
}
