﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserAttack_01 : LaserAttacks
{
    public float startProgression;
    public float endProgression;

    private float progression;
    private float proPro;

    public ParticleSystem ParticleSystem;

    public LaserAttack_01(BossBehaviour bossBehaviour, GameObject laser,float laserSize , float startProgression = 0f, float endProgression = 1f, float timeTaken = 1f) 
        : base(bossBehaviour, laser, timeTaken, laserSize)
    {
        this.startProgression = startProgression;
        this.endProgression = endProgression;
        
        if (laser.TryGetComponent(out LaserScript laserScript))
        {
            ParticleSystem = laserScript.laserParticles;
        }
    }

    public override void Tick()
    {
        //Debug.Log(proPro + " progression : " + progression);
        
        if (!laser || proPro >= 0.99f)
        {
            Remove();
        }
        
        base.Tick();
        progression = Mathf.Lerp(startProgression, endProgression, proPro);
        proPro += Time.deltaTime / timeTaken;

        bossBehaviour.boss.LaserCircle(progression, laser.GetComponent<LineRenderer>(), 
            bossBehaviour.boss.rangeLaser1, bossBehaviour.boss.rangeLaser2, laserSize, ParticleSystem);
    }
}
