﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class LaserAttacks
{
    public BossBehaviour bossBehaviour;
    public GameObject laser;

    public float timeTaken;
    public float laserSize;

    public Damage.DamageType DamageType;

    private float x;
    
    public float timeLeft;
    
    public delegate void OnRemove();
    public OnRemove triggerOnRemove;

    public LaserAttacks(BossBehaviour bossBehaviour, GameObject laser, float timeTaken, float laserSize)
    {
        this.bossBehaviour = bossBehaviour;
        this.laser = laser;
        this.laserSize = laserSize;
        
        this.timeTaken = timeTaken;
        timeLeft = timeTaken;

        DamageType = this.bossBehaviour.boss.resType;

        switch (DamageType)
        {
            case Damage.DamageType.Fire :
                laser.GetComponent<LineRenderer>().material = bossBehaviour.boss.mat_r;
                break;
            case Damage.DamageType.Grass :
                laser.GetComponent<LineRenderer>().material = bossBehaviour.boss.mat_g;
                break;
            case Damage.DamageType.Water :
                laser.GetComponent<LineRenderer>().material = bossBehaviour.boss.mat_b;
                break;
        }
    }

    public virtual void OnAdded()
    {
        LaserScript laserScript = laser.GetComponent<LaserScript>();
        laserScript.dmg = new Damage(bossBehaviour.boss.laserdmg, DamageType);
        
        laserScript.laserParticles.Play();

        bossBehaviour.boss.SpriteRenderer.sprite = bossBehaviour.boss.spriteAttack;
    }
        
    public virtual void Tick()
    {
        laser?.GetComponent<Renderer>()?.material?.SetTextureOffset("_MainTex", new Vector2(x, 0));
        x += -Time.deltaTime * 0.5f;
        
        timeLeft -= Time.deltaTime / timeTaken;
    }

    public virtual void Remove()
    {
        triggerOnRemove?.Invoke();
        bossBehaviour.boss.SpriteRenderer.sprite = bossBehaviour.boss.spriteAttack;
        
        if (laser)
        {
            MonoBehaviour.Destroy(laser);
        }
        bossBehaviour.LaserAttacks.Remove(this);
    }

    public LaserAttacks ShallowCopy()
    {
        return  (LaserAttacks) MemberwiseClone();
    }
}
