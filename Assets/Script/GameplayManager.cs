﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameplayManager : MonoBehaviour
{
  //  public List<GameObject>;
    public GameObject boss;

    public GameObject panelEndGame;
    public TextMeshProUGUI textEndGame;
    
    public GameObject p1;
    public GameObject p2;
    public GameObject p3;
    
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (boss.GetComponent<Boss>().hp <= 0)
        {
            panelEndGame.SetActive(true);
            textEndGame.SetText("VICTOIRE");
        }
        if (p1.GetComponent<PlayerSystem>().isDead == false&& p2.GetComponent<PlayerSystem>().isDead == false && p3.GetComponent<PlayerSystem>().isDead == false)
        {
            panelEndGame.SetActive(true);
            textEndGame.SetText("PERDU");
        }
        
    }
    public void retryButton()
    {
        SceneManager.LoadScene(0);
        Debug.Log("retry scene");
    }
}
