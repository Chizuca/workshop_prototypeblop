﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LaserScript : MonoBehaviour
{
    public Damage dmg;

    public ParticleSystem laserParticles;

    void Start()
    {
        
    }
    
    void Update()
    {
        
    }

    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<PlayerSystem>())
        {
            //Debug.Log("Laser coll");
            col.GetComponent<PlayerSystem>().TakeDamageplayer((int) dmg.dmg);
        }
    }
}
