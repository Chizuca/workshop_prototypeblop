﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Mime;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;


public class PlayerSystem : MonoBehaviour
{
    public GameObject element;
    public GameObject elementPoint;
    public bool elementBool;
    public KeyCode shootT, leftT, rightT, shieldT;
    public GameObject target;
    public int health;
    public bool isDead, startTimer;
    public float resTimer;
    public float maxTimer;
    public int speed;
    public bool canDamageOn;


        //test !!!
        public GameObject planet;
        public float maxGravity;
        public float maxGravityDist;
        public GameObject shield;

        private float lookAngle;
        private Vector3 lookDirection;
        public Rigidbody2D rb;

        public Slider health_bar;
        public Slider timer_bar;
        public int maxHealth;
        
        //Bar de vie
        public Sprite spriteMort;
        public Sprite spriteLife;

        public float shieldTimer, shieldTimerMax;
        
        
        private void Start()
        {
            health = maxHealth;
            health_bar.maxValue = maxHealth;
            timer_bar.maxValue = maxTimer;
        }

        void Update()
        {
        float dist = Vector2.Distance(planet.transform.position, transform.position);
        Vector3 v = planet.transform.position - transform.position;
        rb.AddForce(v.normalized * ((1.0f - dist / maxGravity) * maxGravity));
        lookDirection = planet.transform.position - transform.position;
        lookAngle = Mathf.Atan2(lookDirection.y, lookDirection.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(0f, 0f, lookAngle);
        
        health_bar.value = health;

        if (Input.GetKey(leftT) && isDead)
        {
            transform.RotateAround(target.transform.position, Vector3.forward, speed * Time.deltaTime);
        }
        if (Input.GetKey(rightT) && isDead)
        {
            transform.RotateAround(target.transform.position, Vector3.back, speed * Time.deltaTime);
        }
        if (Input.GetKeyDown(shootT)&& elementBool && isDead)
        {
            elementBool = false;
            StartCoroutine(ShootFire());
        }
        if(Input.GetKey(shieldT)&& isDead)
        {
            if (shieldTimer >= shieldTimerMax)
            {
                shield.SetActive(true);
            }

            if (shield.activeSelf)
            {
                shieldTimer -= Time.deltaTime;
            }
            if (shieldTimer <= 0)
            {
                shield.SetActive(false);
            }
        }

        if (Input.GetKeyUp(shieldT) && isDead)
        {
            shield.SetActive(false);
        }

        if (shieldTimer < shieldTimerMax && !shield.activeSelf)
        {
            shieldTimer += Time.deltaTime/4;
        }
        if (startTimer && !isDead)
        {
            if (resTimer >= maxTimer)
            {
                health_bar.gameObject.SetActive(true);
                timer_bar.gameObject.SetActive(false);
                health = maxHealth;
                gameObject.GetComponent<SpriteRenderer>().sprite = spriteLife;
                gameObject.GetComponent<SpriteRenderer>().color = Color.white;
                resTimer = 0;
                isDead = true;
            }
        }
        if (health <= 0)
        {
            health_bar.gameObject.SetActive(false);
            timer_bar.gameObject.SetActive(true);
            gameObject.GetComponent<SpriteRenderer>().sprite = spriteMort;
            gameObject.GetComponent<SpriteRenderer>().color = Color.grey;
            isDead = false;
        }
    }
    IEnumerator ShootFire()
    {
        Instantiate(element, elementPoint.transform.position, elementPoint.transform.rotation);
        yield return new WaitForSeconds(0.2f);
        elementBool = true;
    }

    public void TakeDamageplayer(int damage)
    {
        if (!canDamageOn)
        {
            Camera.main.DOShakePosition(0.5f, 0.3f);
            GetComponent<SpriteRenderer>().DOColor(Color.red, 0.5f).SetLoops(3, LoopType.Yoyo).OnComplete(comebackWhite);
            health -= damage;
            canDamageOn = true;
        }
    }
    public void comebackWhite()
    {
        GetComponent<SpriteRenderer>().color = Color.white;
        canDamageOn = false;
    }
    private void OnCollisionStay2D(Collision2D other1)
    {
        if (other1.gameObject.CompareTag("Player") && !isDead)
        {
            startTimer = true;
        }
    }

    private void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.CompareTag("Player") && !isDead)
        {
            startTimer = false;
        }
    }
}

