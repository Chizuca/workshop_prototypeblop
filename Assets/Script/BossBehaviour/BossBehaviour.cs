﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour
{
    public Boss boss;
    public float timerAction;
    public float difficulty;
    
    public List<LaserAttacks> LaserAttacks = new List<LaserAttacks>();

    protected float timer;

    public bool ending;

    public BossBehaviour(Boss boss, float timerAction, float difficulty = 1)
    {
        this.boss = boss;
        this.timerAction = timerAction;
        this.difficulty = difficulty;
    }

    public virtual void OnAdded()
    {
        timer = timerAction;
    }
    
    public virtual void Tick()
    {
        if (LaserAttacks.Capacity != 0)
        {
            for (int i = 0; i < LaserAttacks.Count; i++)
            {
                LaserAttacks[i].Tick();
            }
        }
    }

    public virtual void OnRemove()
    {
        foreach (var laser in LaserAttacks)
        {
            MonoBehaviour.Destroy(laser.laser);
        }
    }
    
    public LaserAttacks AddLaserAttack(LaserAttacks laserAttack)
    {
        LaserAttacks.Add(laserAttack);
        laserAttack.OnAdded();
        return laserAttack;
    }
}
