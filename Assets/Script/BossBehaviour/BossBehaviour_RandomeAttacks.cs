﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossBehaviour_RandomeAttacks : BossBehaviour
{
    public float timerRandomness;
    
    public BossBehaviour_RandomeAttacks(Boss boss, float timerAction, float timerRandomness, float difficulty = 1) : base(boss, timerAction, difficulty)
    {
        this.timerRandomness = timerRandomness;
    }
    
    public override void Tick()
    {
        base.Tick();

        timer -= Time.deltaTime;

        if (timer <= 0 && !ending)
        {
            int rand = Random.Range(0, 2);
            
            float randStart = Random.Range(0f, 1f);
            float randEnd = randStart + Random.Range(0f, 1f);

            switch (rand)
            {
                case 0 :
                    //Debug.Log("Cast Laser");
                    float randSize = Random.Range(0.3f, 1f) * difficulty;
                    float randTime = Random.Range(0.7f, 1.5f) * (1 + Mathf.Abs(randStart - randEnd) * 1.2f) / difficulty;
                    //randTime = Mathf.Clamp(randTime, 0.7f / difficulty, 1.5f / difficulty);
                    //Debug.Log(randTime + " " + Mathf.Abs(randStart - randEnd));

                    AddLaserAttack(new PrepareLaserAttack(this, MonoBehaviour.Instantiate(boss.warningLaserLine, null), randSize, 
                        new LaserAttack_01(this, MonoBehaviour.Instantiate(boss.laserLine, null), randSize, randStart, randEnd, randTime)));
                    
                    break;
                case 1 :
                    float randTimeBetweenBullets = Random.Range(0.1f, 0.3f) * difficulty;
                    float randSpaceBetweenBullets = Random.Range(0.1f, 0.3f) * difficulty;
                    float randBullets = Random.Range(8, 14) * difficulty;
                    float randPass = Random.Range(0, 3) * difficulty;
                    bool randDirection = Random.Range(0, 1) == 1;

                    boss.StartCoroutine(boss.BurstShoot((int) randBullets, (int) randBullets, randStart, randEnd, 1f, 
                        randTimeBetweenBullets, randSpaceBetweenBullets, randDirection, 0f, (int) randPass));
                    break;
            }
            
            timer = timerAction + Random.Range(-timerRandomness, timerRandomness);
            //Debug.Log(timer);
        }
    }
}
