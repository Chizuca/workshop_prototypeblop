﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class BossBehaviour_Pattern : BossBehaviour
{
    public float timerRandomness;
    public delegate void Pattern();
    public List<Pattern> Patterns = new List<Pattern>();

    public bool patterning;

    public BossBehaviour_Pattern(Boss boss, float timerAction, float timerRandomness, float difficulty = 1) : base(boss, timerAction, difficulty)
    {
        this.timerRandomness = timerRandomness;
    }

    public override void OnAdded()
    {
        base.OnAdded();
        
        Pattern pattern_01 = null;
        pattern_01 += Laser_01;
        pattern_01 += Shoot_01;
        Patterns.Add(pattern_01);
        
        Pattern pattern_02 = null;
        pattern_02 += Laser_02;
        pattern_02 += Shoot_01;
        Patterns.Add(pattern_02);
        
        Pattern pattern_03 = null;
        pattern_03 += Laser_03;
        Patterns.Add(pattern_03);
    }

    public override void Tick()
    {
        base.Tick();
        if (!patterning || difficulty >= 1.5f)
        {
            timer -= Time.deltaTime;
        }

        if (timer <= 0 && !ending)
        {
            int rand = Random.Range(0, Patterns.Count);

            Patterns[rand]?.Invoke();
            
            timer = timerAction + Random.Range(-timerRandomness, timerRandomness);
        }
    }

    public void Laser_01()
    {
        patterning = true;
        
        ((PrepareLaserAttack) AddLaserAttack(new PrepareLaserAttack(this, MonoBehaviour.Instantiate(boss.warningLaserLine, null), 0.8f, 
            new LaserAttack_01(this, MonoBehaviour.Instantiate(boss.laserLine, null), 
                0.8f, 0f, 0.25f, 1.5f)))).nextAttack.triggerOnRemove += () => SetPatterning(false);
        
        AddLaserAttack(new PrepareLaserAttack(this, MonoBehaviour.Instantiate(boss.warningLaserLine, null), 0.8f, 
            new LaserAttack_01(this, MonoBehaviour.Instantiate(boss.laserLine, null), 0.8f, 0.5f, 0.75f, 1.5f)));
    }

    public void Laser_02()
    {
        patterning = true;
        
        float randProgressStart = Random.Range(0, 1f);
        float randProgressEnd = randProgressStart + 0.25f;
        
        ((PrepareLaserAttack) AddLaserAttack(new PrepareLaserAttack(this, MonoBehaviour.Instantiate(boss.warningLaserLine, null), 0.8f, 
            new LaserAttack_01(this, MonoBehaviour.Instantiate(boss.laserLine, null), 0.8f, randProgressStart, randProgressEnd, 1.5f))))
            .nextAttack.triggerOnRemove += () => SetPatterning(false);
    }

    public void Laser_03()
    {
        patterning = true;
        
        ((PrepareLaserAttack) AddLaserAttack(new PrepareLaserAttack(
            this, MonoBehaviour.Instantiate(boss.warningLaserLine, null), 0.6f, new LaserAttack_01(
                this, MonoBehaviour.Instantiate(boss.laserLine, null), 0.6f, 0.25f, 0.5f, 1.5f)))).nextAttack.triggerOnRemove += Laser_03_02;

        AddLaserAttack(new PrepareLaserAttack(this, MonoBehaviour.Instantiate(boss.warningLaserLine, null), 0.6f,
            new LaserAttack_01(this, MonoBehaviour.Instantiate(boss.laserLine, null),
                0.6f, 0.625f, 0.875f, 1.5f)));
        
        AddLaserAttack(new PrepareLaserAttack(this, MonoBehaviour.Instantiate(boss.warningLaserLine, null), 0.6f, 
            new LaserAttack_01(this, MonoBehaviour.Instantiate(boss.laserLine, null), 0.6f, 0.875f, 1.125f, 1.5f)));
    }

    public void Laser_03_02()
    {
        AddLaserAttack(new LaserAttack_01(this, MonoBehaviour.Instantiate(boss.laserLine, null), 
            0.6f, 0.5f, 0.25f, 1.5f)).triggerOnRemove += () => SetPatterning(false);
        
        AddLaserAttack(new LaserAttack_01(this, MonoBehaviour.Instantiate(boss.laserLine, null), 
            0.6f, 0.875f, 0.625f, 1.5f));
        
        AddLaserAttack(new LaserAttack_01(this, MonoBehaviour.Instantiate(boss.laserLine, null), 
            0.6f, 1.125f, 0.875f, 1.5f));
    }

    public void Shoot_01()
    {
        boss.StartCoroutine(boss.BurstShoot(5, 5, 0.25f, 0.5f, 1f, 
            0.3f, 0.1f, true, 0f, 1));
        
        boss.StartCoroutine(boss.BurstShoot(5, 5, 0.75f, 1f, 1f, 
            0.3f, 0.1f, true, 0f, 1));
    }

    public void Shoot_02()
    {
        boss.StartCoroutine(boss.BurstShoot(5, 5, 0.25f, 0.5f, 1f, 
            0.3f, 0.1f, true, 0f, 1));
    }

    public void SetPatterning(bool b)
    {
        patterning = b;
    }
}