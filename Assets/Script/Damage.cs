﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Damage
{
   public float dmg;
   
   public enum DamageType {Fire, Water, Grass }
   
   public DamageType damageType;

   public Damage(float dmg, DamageType type)
   {
      this.dmg = dmg;
      damageType = type;
   }
}
