﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Boss_Bullet : MonoBehaviour
{
    public Vector2 direction;
    public float speed;
    public float dmg;
    
    void Start()
    {
        Destroy(gameObject, 3f);
    }
    
    void Update()
    {
        transform.Translate(direction * speed);
    }
    
    public void OnTriggerEnter2D(Collider2D col)
    {
        if (col.GetComponent<PlayerSystem>())
        {
            Destroy(gameObject);
            col.GetComponent<PlayerSystem>().TakeDamageplayer((int) dmg);
        }
        else if (col.CompareTag("Bouclier"))
        {
            Destroy(gameObject);
        }
    }
}
