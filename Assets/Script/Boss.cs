﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

public class Boss : MonoBehaviour
{
    [Header("Boss")]
    public Damage.DamageType resType;

    public float rollColorTimerDuration = 2f;

    private float timer;
    
    public float randomTimeColor = 0.5f;

    public int nbrOfChangeColorbeforChangeBehaviour;
    private int nbrOfChangeColorbeforChangeBehaviourleft;

    public float maxHp;

    [NonSerialized]
    public float hp;

    public SpriteRenderer SpriteRenderer;
    public Sprite spriteIdle;
    public Sprite spriteAttack;
    
    public GameObject bulletBoss;

    public BossBehaviour BossBehaviour;

    public LayerMask playerLayer;

    [Header("UI")]

    public GameObject hpScaler;
    public GameObject hpBar;

    public Transform hpPos;

    [Header("Laser")] 
    public Material mat_r;
    public Material mat_g;
    public Material mat_b;

    public GameObject laserLine;
    public GameObject warningLaserLine;

    public float laserdmg;

    public float rangeLaser1;
    public float rangeLaser2;

    public Color fire_color;
    public Color water_color;
    public Color dirt_color;
    void Start()
    {
        hp = maxHp;
        nbrOfChangeColorbeforChangeBehaviourleft = nbrOfChangeColorbeforChangeBehaviour;
        
        //SetBehaviourState(new BossBehaviour_RandomeAttacks(this, 4f, 1f));
        SetBehaviourState(new BossBehaviour_Pattern(this, 1f, 0.5f));
    }

    void Update()
    {
        timer -= Time.deltaTime;
        
        if (timer <= 0)
        {
            RollColor();

            float rand = Random.Range(-randomTimeColor, randomTimeColor);

            timer = rollColorTimerDuration + rand;
            //Debug.Log("timer : " + timer);

            nbrOfChangeColorbeforChangeBehaviourleft -= 1;
            if (nbrOfChangeColorbeforChangeBehaviourleft <= 0)
            {
                ChangeBehaviour();
                nbrOfChangeColorbeforChangeBehaviourleft = nbrOfChangeColorbeforChangeBehaviour;
            }
        }

        hp = Mathf.Clamp(hp, 0, maxHp);
        
        hpScaler.transform.localScale = new Vector3(hp / maxHp, hpScaler.transform.localScale.y, hpScaler.transform.localScale.z);

        hpBar.GetComponent<RectTransform>().position = Camera.main.WorldToScreenPoint(hpPos.position);
        
        BossBehaviour?.Tick();
    }

    public void ChangeBehaviour()
    {
        /*if (BossBehaviour.GetType() == typeof(BossBehaviour_Pattern))
        {
            if (BossBehaviour.LaserAttacks.Count <= 0)
            {
                SetBehaviourState(new BossBehaviour_RandomeAttacks(this, 4f, 1f));
            }
            else
            {
                BossBehaviour.ending = true;
                foreach (var laser in ((BossBehaviour_Pattern) BossBehaviour).LaserAttacks)
                {
                    if (laser.GetType() != typeof(PrepareLaserAttack))
                    {
                        laser.triggerOnRemove += () => SetBehaviourState(new BossBehaviour_RandomeAttacks(this, 4f, 1f));
                    }
                    else
                    {
                        ((PrepareLaserAttack) laser).nextAttack.triggerOnRemove += () => 
                            SetBehaviourState(new BossBehaviour_RandomeAttacks(this, 4f, 1f));
                    }
                }
            }
        }
        else
        {
            if (BossBehaviour.LaserAttacks.Count <= 0)
            {
                SetBehaviourState(new BossBehaviour_RandomeAttacks(this, 4f, 1f));
            }
            else
            {
                BossBehaviour.ending = true;
                foreach (var laser in ((BossBehaviour_RandomeAttacks) BossBehaviour).LaserAttacks)
                {
                    if (laser.GetType() != typeof(PrepareLaserAttack))
                    {
                        laser.triggerOnRemove += () => SetBehaviourState(new BossBehaviour_Pattern(this, 1f, 0.5f));
                    }
                    else
                    {
                        ((PrepareLaserAttack) laser).nextAttack.triggerOnRemove += () => SetBehaviourState(new BossBehaviour_Pattern(this, 1f, 0.5f));
                    }
                }
            }
        }*/
        
        float shortestTimeLeft = Mathf.Infinity;
        LaserAttacks laserAttack = null;

        foreach (var laser in  BossBehaviour.LaserAttacks)
        {
            if (laser.timeLeft < shortestTimeLeft)
            {
                laserAttack = laser;
                shortestTimeLeft = laser.timeLeft;
                BossBehaviour.ending = true;
            }
        }

        if (laserAttack != null)
        {
            if (laserAttack.GetType() == typeof(PrepareLaserAttack))
            {
                ((PrepareLaserAttack) laserAttack).nextAttack.triggerOnRemove += () => SetBehaviourState(AlternateState(BossBehaviour));
            }
            else
            {
                laserAttack.triggerOnRemove += () => SetBehaviourState(AlternateState(BossBehaviour));
            }
        }
        else
        {
            SetBehaviourState(AlternateState(BossBehaviour));
        }
       
    }

    public BossBehaviour AlternateState(BossBehaviour currentBehaviour)
    {
        if (currentBehaviour.GetType() == typeof(BossBehaviour_Pattern))
        {
            return new BossBehaviour_RandomeAttacks(this, 4f, 1f);
        }
        else
        {
            return new BossBehaviour_Pattern(this, 1f, 0.5f);
        }
    }

    public void RollColor()
    {
        int rand = Random.Range(0, 3);

        switch (rand)
        {
            case 0 :
                if (resType == Damage.DamageType.Fire)
                {
                    RollColor();
                    //Debug.Log("Same Color");
                    return;
                }
                
                resType = Damage.DamageType.Fire;
                GetComponent<SpriteRenderer>().color = fire_color;
                break;
            case 1 :
                if (resType == Damage.DamageType.Water)
                {
                    RollColor();
                    //Debug.Log("Same Color");
                    return;
                }
                
                resType = Damage.DamageType.Water;
                GetComponent<SpriteRenderer>().color = water_color;
                break;
            case 2 :
                if (resType == Damage.DamageType.Grass)
                {
                    RollColor();
                    //Debug.Log("Same Color");
                    return;
                }
                
                resType = Damage.DamageType.Grass;
                GetComponent<SpriteRenderer>().color = dirt_color;
                break;
        }
    }

    public void TakeDamage(Damage damage)
    {
        if (damage.damageType == resType)
        {
            hp += damage.dmg;
        }else if (resType == Damage.DamageType.Fire && damage.damageType == Damage.DamageType.Grass || resType == Damage.DamageType.Grass && damage.damageType == Damage.DamageType.Water ||
                  resType == Damage.DamageType.Water && damage.damageType == Damage.DamageType.Fire)
        {
            Debug.Log("Take 0 dmg");
        }
        else
        {
            hp -= damage.dmg;
        }
        Debug.Log(hp);
    }

    public void SetBehaviourState(BossBehaviour behaviour)
    {
        BossBehaviour?.OnRemove();
        BossBehaviour = behaviour;
        BossBehaviour?.OnAdded();
        
        //Debug.Log("Set behaviour : " + BossBehaviour?.GetType());
    }
    
    /*public void Laser(Vector2 pos, LineRenderer line, float laserDuration,float range1, float range, float laserSize, bool direction)
    {
        Destroy(line.gameObject, laserDuration);
        line.GetComponent<LaserScript>().dmg = laserdmg;
        
        if (direction)
        {
            line.SetPosition(0, pos.normalized * range1);
            line.SetPosition(1, pos.normalized * range);
        }
        else {
            Vector2 dir = pos - new Vector2(transform.position.x, transform.position.y);
            
            line.SetPosition(0, dir.normalized * range1);
            line.SetPosition(1, dir.normalized * range);
        }

        line.startWidth = laserSize;

        EdgeCollider2D edgeCollider2D =  line.GetComponent<EdgeCollider2D>();

        edgeCollider2D.edgeRadius = laserSize;

        Vector3[] linePos = new Vector3[line.positionCount];

        line.GetPositions(linePos);
        
        List<Vector2> poz = new List<Vector2>();
        
        foreach (var position in linePos)
        {
            poz.Add(new Vector2(position.x, position.y));
        }

        edgeCollider2D.points = poz.ToArray();
    }*/

    public void LaserCircle(float progression, LineRenderer lineRenderer, float range1,
        float range, float laserSize, ParticleSystem laserParticle)
    {
        float x = Mathf.Cos(progression * (Mathf.PI * 2f)) * range1;
        float y = Mathf.Sin(progression *(Mathf.PI * 2f)) * range1;
        
        Vector2 dir = new Vector2(x, y).normalized;
        
        lineRenderer.SetPosition(0, new Vector2(transform.position.x, transform.position.y) + dir * range1);

        RaycastHit2D hit = Physics2D.Raycast(transform.position, dir, range, playerLayer);

        if (hit)
        {
            lineRenderer.SetPosition(1, hit.point);

            if (laserParticle)
            {
                laserParticle.transform.position = hit.point;
            }
        }
        else
        {
            lineRenderer.SetPosition(1, new Vector2(transform.position.x, transform.position.y) + dir * range);

            if (laserParticle)
            {
                Vector2 screenBottomLeft = Camera.main.ScreenToWorldPoint(new Vector3(0, 0, 0));
                Vector2 screenTopRight = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight, 0));
            
                laserParticle.transform.position = Vector3.Project(new Vector3(
                    Mathf.Clamp(transform.position.x + x * range, screenBottomLeft.x, screenTopRight.x),
                    Mathf.Clamp(transform.position.y + y * range, screenBottomLeft.y, screenTopRight.y), 0f), new Vector3(dir.x, dir.y, 0));
            }
        }

        //Laser Particles
        if (laserParticle)
        {
            Vector2 dir2 = -dir;
            
            float distance = dir2.magnitude;
            Vector2 direction = dir2 / distance;
            direction.Normalize();
            
            float rotationZ = Mathf.Atan2(dir2.y, dir2.x) * Mathf.Rad2Deg;

            laserParticle.transform.rotation = Quaternion.Euler(0.0f, 0.0f, rotationZ - 90f);
        }
        
        EdgeCollider2D edgeCollider2D =  lineRenderer.GetComponent<EdgeCollider2D>();
        
        lineRenderer.startWidth = laserSize;

        if (edgeCollider2D)
        {
            edgeCollider2D.edgeRadius = laserSize / 2;
        
            //Set edgecollider points to line Points
            Vector3[] linePos = new Vector3[lineRenderer.positionCount];

            lineRenderer.GetPositions(linePos);
        
            List<Vector2> poz = new List<Vector2>();
        
            foreach (var position in linePos)
            {
                poz.Add(new Vector2(position.x, position.y));
            }

            edgeCollider2D.points = poz.ToArray();
        }
    }

    public float TargetToProgression(Vector2 Target)
    {
        Debug.Log("TargetToProgression");
        float angle;
        float progression;

        angle = Vector2.Angle(gameObject.transform.position, Target);

        progression = angle / 360f;
        
        return progression;
    }
    
    public IEnumerator BurstShoot(int nbrOfBullets, int nbrOfBulletLeft, float progressionStart, float progressionEnd, float rangeStart, float timeBetweenBullet,
        float spaceBetweenBullets, bool left = true, float progress = 0f, int pass = 0)
    {
        yield return new WaitForSeconds(timeBetweenBullet);

        SpriteRenderer.sprite = spriteAttack;
        
        Boss_Bullet bullet = Instantiate(bulletBoss, null).GetComponent<Boss_Bullet>();

        float progression = 0f;

        progression = progressionStart + progress;
        progression = ClampLoop(progression, 0f, 1f);
        
        //Debug.Log(progression);

        float x = Mathf.Cos(progression * (Mathf.PI * 2f)) * rangeStart;
        float y = Mathf.Sin(progression *(Mathf.PI * 2f)) * rangeStart;

        bullet.transform.position = transform.position;
        bullet.direction = new Vector2(x, y);
        bullet.dmg = laserdmg;
        bullet.speed = 0.03f;

        if (!Mathf.Approximately(Mathf.Abs(progressionEnd - progression), 0))
        {
            if (left)
            {
                progress += spaceBetweenBullets;
            }
            else
            {
                progress -= spaceBetweenBullets;
            }
            
            progress = (float) Math.Round(progress, 2);
            progress = ClampLoop(progress, 0f, 1f);
        }

        nbrOfBulletLeft -= 1;

        if (nbrOfBulletLeft > 0)
        {
            StartCoroutine(BurstShoot(nbrOfBullets, nbrOfBulletLeft, progressionStart, progressionEnd, rangeStart, timeBetweenBullet, spaceBetweenBullets, left, progress, pass));
        }
        else if (pass > 0)
        {
            //Debug.Log("Reverse");
            StartCoroutine(BurstShoot(nbrOfBullets, nbrOfBullets, progressionEnd, progressionStart,
                rangeStart, timeBetweenBullet, spaceBetweenBullets, !left, 0f, pass - 1));
        }
        else {
             SpriteRenderer.sprite = spriteIdle;
            //Debug.Log("BurstShoot end");
        }
    }

    public float ClampLoop(float nbr, float min, float max)
    {
        
        if (nbr > max)
        {
            float diff = nbr - max;
            diff = (float) Math.Round(diff, 2);
            //Debug.Log(diff);
            nbr = min + diff;
        }

        return nbr;
    }
}
