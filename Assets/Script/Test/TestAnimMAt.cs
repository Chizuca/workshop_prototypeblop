﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestAnimMAt : MonoBehaviour
{
    public float x;
    public float y;
    
    void Start()
    {
        Debug.Log(GetComponent<Renderer>().GetType());
    }
    
    void Update()
    {
        GetComponent<Renderer>()?.material?.SetTextureOffset("_MainTex", new Vector2(x, y));
        x += -Time.deltaTime * 3f;
        //y += Time.deltaTime * 10f;
    }
}
