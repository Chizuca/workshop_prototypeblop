﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public Rigidbody2D rb;
    public int speed;
    public Damage.DamageType restypeb;
    public Collider2D boss;
    public float degat;
    public GameObject impact_effect;

    void Update()
    {
        rb.velocity = transform.right * speed;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        Instantiate(impact_effect, transform.position,Quaternion.identity);
        Debug.Log(other);
        other.GetComponent<Boss>().TakeDamage(new Damage(degat,restypeb));
        Destroy(gameObject);
    }
}

